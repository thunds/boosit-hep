/**
 * Created by julien on 25/01/2015.
 */
var registrationApp = angular.module('regist', []);

registrationApp.controller('reg', function ($scope)
{
    var socket = io("ws://localhost:8090");
    $scope.registration = {};
    $scope.isNotValid = false;
    $scope.errmsg = "";

    socket.on("Registration", function(errMsg)
    {
        $scope.printError(errMsg);
    });

    $scope.printError = function(errMsg)
    {
        if (errMsg == undefined)
            document.location.href="/";
        else
        {
            $scope.isNotValid = true;
            $scope.errmsg = errMsg;
            console.log($scope.errmsg);
            $scope.$digest();
        }
    }

    $scope.validate = function()
    {
        if (fieldValid() == true)
        {
            $scope.isNotValid = false;
            $scope.errmsg = "";
            socket.emit("Registration", $scope.registration);
        }
        else
        {
            $scope.isNotValid = true;
            $scope.errmsg = "You have to fill every inputs!";
        }
    };
    function fieldValid()
    {
        if ($scope.registration.firstName && $scope.registration.lastName &&
            $scope.registration.businessName && $scope.registration.phoneNb &&
            $scope.registration.siretNb && $scope.registration.addr && $scope.registration.zipCode &&
            $scope.registration.city && $scope.registration.email && $scope.registration.password &&
            $scope.registration.confPassword && $scope.registration.password == $scope.registration.confPassword)
            return true;
        return false;
    }
});

/**
 * Created by yous on 24/01/2015.
 */
module.exports = function(informations, socket)
{
    var Firebase = require("firebase");

    var ref = new Firebase("https://hep-zone.firebaseio.com/dev/users_site/" + new Date().getTime());
    ref.createUser({
        email: informations.email,
        password: informations.password
    }, function(error) {
        if (error) {
            switch (error.code) {
                case "EMAIL_TAKEN":
                    socket.emit("Registration", "The new user account cannot be created because the email already exist.");
                    console.log("The new user account cannot be created because the email already exist.");
                    break;
                case "INVALID_EMAIL":
                    socket.emit("Registration", "The specified email is not a valid email.");
                    console.log("The specified email is not a valid email.");
                    break;
                default:
                    socket.emit("Registration", "Error creating user:", error);
                    console.log("Error creating user:", error);
            }
        }
        else {
            console.log("User account created successfully!");
            ref.set({ Name: informations.lastName, Mail: informations.email });
            socket.emit("Registration", undefined);
        }
    });
};

ref.authWithPassword({
    "email": "bobtony@firebase.com",
    "password": "correcthorsebatterystaple"
}, function(error, authData) {
    if (error) {
        console.log("Login Failed!", error);
    } else {
        console.log("Authenticated successfully with payload:", authData);
    }
});
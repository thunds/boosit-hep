module.exports = function(app)
{
    module = {};

    module.io = require("socket.io").listen(app.listen(8090));

    var CookieParser = require("cookie-parser");
    var Session = require("express-session");
    var memoryStore = Session.MemoryStore;
    var SessionStore = new memoryStore();

    app.SessionStore = SessionStore;
    app.use(CookieParser());
    app.use(Session({store: SessionStore, cookie: {secure: false}, saveUninitialized: true, resave: true, secret: "@lfred"}));

    var SessionSocket = require("./session.socket.io-express4");
    module.SessionSockets = new SessionSocket(module.io, SessionStore, CookieParser());

    module.SessionSockets.on("connection", function (err, socket, session)
    {
        require("../model/Users.js")(socket);
    });

    return module;
}
/**
 * Created by julien on 24/01/2015.
 */
module.exports = function(engine)
{
    var conf_swig = {cache: false, varControls: ["{[", "]}"]};
    engine.swig = require("swig");
    engine.swig.setDefaults({ cache: false });

//app.SessionStore = SessionStore;
    engine.app.engine('html', engine.swig.renderFile);
    engine.app.set('view engine', 'html');
    engine.app.set('view cache', false);

//    app.use(CookieParser());
//  app.use(Session({store: SessionStore, cookie: {secure: false}, saveUninitialized: true, resave: true, secret: "@lfred"}));

    function clear_auth_cookies(req, res)
    {
        req.session.user = req.cookies.username = undefined;
        req.session.deco = req.cookies.autoauth = undefined;
        res.clearCookie("username");
        res.clearCookie("autoauth");
    }

// set and unset the auth Cookie
    engine.app.use(function (req, res, next){
        if (req.session && req.session.deco)
        {
            clear_auth_cookies(req, res);
            next();
        }
        else if (req.session && req.session.user && req.session.autoauth)
        {
            res.cookie("username", req.session.user.Email, { maxAge: 900000, httpOnly: true });
            res.cookie("autoauth", req.session.user.AuthCookie, { maxAge: 900000, httpOnly: true });
            next();
        }
        else
            next();
    });
    return engine;
};

/**
 * Created by julien on 24/01/2015.
 */
var engine = {};
engine.express = require("express");
engine.app = engine.express();

engine.app.set('views', __dirname + '/views');
engine.app.use(engine.express.static("public", __dirname + "/public"));
engine.app = require("./routes/routes.js")(engine);
engine = require("./config/configuration.js")(engine);

require("./config/websocket.js")(engine.app);
